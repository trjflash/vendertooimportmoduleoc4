<?php
namespace Opencart\Admin\Model\Extension\VenderCatalog\module;

class VenderCatalog extends \Opencart\System\Engine\Model {
	
	public function getModuleCode() {
        $query = $this->db->query("SELECT `module_id` FROM `" . DB_PREFIX . "module` WHERE `name` = 'VenderImport'");

        return $query->rows;
    }

	public function createTable(){
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "vender_import_cron_data` (
			`id` INT(11) NOT NULL AUTO_INCREMENT,
			`data` TEXT NOT NULL,
			`creation_time` DATETIME NOT NULL,
			`execution_time` TIME NOT NULL,
			`task_code` INT(11) NOT NULL,
			`is_active` TINYINT(1) NOT NULL,
			`clear_catalog` TINYINT(1) NOT NULL,
			PRIMARY KEY (`id`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;");
	}

	public function addRows(){
	    $seo_url_table = "ALTER TABLE `" . DB_PREFIX . "seo_url` ADD `query` VARCHAR(255) NOT NULL AFTER `sort_order`;";
	    $category_table = "ALTER TABLE `" . DB_PREFIX . "category` ADD `vender_cat_id` INT NOT NULL AFTER `parent_id`;";
		$this->db->query($seo_url_table);
		$this->db->query($category_table);
	}
	
	public function dropTable()
    {
        $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "vender_import_cron_data`;");
    }
	
	
    public function addTopCategory(): bool {
		$this->db->query("INSERT INTO " . DB_PREFIX . "category SET category_id = '0',  parent_id = '0', `top` = '1',status = '1', date_modified = NOW(), date_added = NOW()");
		$category_id = $this->db->getLastId();
		$this->db->query("UPDATE `" . DB_PREFIX . "category` SET `category_id` = '1' WHERE `oc_category`.`category_id` = ".$category_id.";");
		$this->db->query("INSERT INTO " . DB_PREFIX . "category_description SET category_id = '1', language_id = '1', name = 'Каталог', description = 'Каталог', meta_title = 'Каталог', meta_description = 'Каталог', meta_keyword = 'Каталог'");
		$this->db->query("INSERT INTO " . DB_PREFIX . "category_to_store SET category_id = '1', store_id = '0'");
		$this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '0', language_id = '1', query = 'category_id=0'");
		$this->db->query("INSERT INTO " . DB_PREFIX . "category_to_layout SET category_id = '1', store_id = '0', layout_id = '1'");


		return true;
	}
    public function addCategory($data): float {
      /*  echo "<pre>";
        print_r($data);
        exit();
        echo "</pre>";*/

		$this->db->query("INSERT INTO " . DB_PREFIX . "category SET parent_id = '" . (int)$data['parent_id'] . "', vender_cat_id = '" . (int)$data['vender_cat_id'] . "', `top` = '" . (isset($data['top']) ? (int)$data['top'] : 0) . "', `column` = '" . (int)$data['column'] . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', date_modified = NOW(), date_added = NOW()");

        $category_id = $this->db->getLastId();

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "category SET image = '" . $this->db->escape($data['image']) . "'");
        }

        foreach ($data['category_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "category_description SET category_id = '" . (int)$category_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }

        // MySQL Hierarchical Data Closure Table Pattern
        $level = 0;

        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$data['parent_id'] . "' ORDER BY `level` ASC");

        foreach ($query->rows as $result) {
            $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$result['path_id'] . "', `level` = '" . (int)$level . "'");

            $level++;
        }

        $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$category_id . "', `level` = '" . (int)$level . "'");

        if (isset($data['category_filter'])) {
            foreach ($data['category_filter'] as $filter_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_filter SET category_id = '" . (int)$category_id . "', filter_id = '" . (int)$filter_id . "'");
            }
        }

        if (isset($data['category_store'])) {
            foreach ($data['category_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_store SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "'");
            }
        }

        if (isset($data['category_seo_url'])) {
            foreach ($data['category_seo_url'] as $store_id => $language) {
                foreach ($language as $language_id => $keyword) {
                    if (!empty($keyword)) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'category_id=" . (int)$category_id . "', keyword = '" . $this->db->escape($keyword) . "'");

                    }
                }
            }
        }

        // Set which layout to use with this category
        if (isset($data['category_layout'])) {
            foreach ($data['category_layout'] as $store_id => $layout_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_layout SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
            }
        }

        $this->cache->delete('category');

        return $category_id;
    }
    public function addProduct($data): float {
        $this->db->query("INSERT INTO " . DB_PREFIX . "product SET model = '" . $this->db->escape($data['model']) . "', sku = '" . $this->db->escape($data['sku']) . "', upc = '" . $this->db->escape($data['upc']) . "', ean = '" . $this->db->escape($data['ean']) . "', jan = '" . $this->db->escape($data['jan']) . "', isbn = '" . $this->db->escape($data['isbn']) . "', mpn = '" . $this->db->escape($data['mpn']) . "', location = '" . $this->db->escape($data['location']) . "', quantity = '" . (int)$data['quantity'] . "', minimum = '" . (int)$data['minimum'] . "', subtract = '" . (int)$data['subtract'] . "', stock_status_id = '" . (int)$data['stock_status_id'] . "', date_available = '" . $this->db->escape($data['date_available']) . "', manufacturer_id = '" . (int)$data['manufacturer_id'] . "', shipping = '" . (int)$data['shipping'] . "', price = '" . (float)$data['price'] . "', points = '" . (int)$data['points'] . "', weight = '" . (float)$data['weight'] . "', weight_class_id = '" . (int)$data['weight_class_id'] . "', length = '" . (float)$data['length'] . "', width = '" . (float)$data['width'] . "', height = '" . (float)$data['height'] . "', length_class_id = '" . (int)$data['length_class_id'] . "', status = '" . (int)$data['status'] . "', tax_class_id = '" . (int)$data['tax_class_id'] . "', sort_order = '" . (int)$data['sort_order'] . "', date_added = NOW(), date_modified = NOW()");

        $product_id = $this->db->getLastId();

        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($data['image']) . "' WHERE product_id = '" . (int)$product_id . "'");
        }

        foreach ($data['product_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', tag = '" . $this->db->escape($value['tag']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }

        if (isset($data['product_store'])) {
            foreach ($data['product_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "'");
            }
        }

        if (isset($data['product_attribute'])) {
            foreach ($data['product_attribute'] as $product_attribute) {
                if ($product_attribute['attribute_id']) {
                    // Removes duplicates
                    $this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "'");

                    foreach ($product_attribute['product_attribute_description'] as $language_id => $product_attribute_description) {
                        $this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "' AND language_id = '" . (int)$language_id . "'");

                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int)$product_id . "', attribute_id = '" . (int)$product_attribute['attribute_id'] . "', language_id = '" . (int)$language_id . "', text = '" .  $this->db->escape($product_attribute_description['text']) . "'");
                    }
                }
            }
        }

        if (isset($data['product_option'])) {
            foreach ($data['product_option'] as $product_option) {
                if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
                    if (isset($product_option['product_option_value'])) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', required = '" . (int)$product_option['required'] . "'");

                        $product_option_id = $this->db->getLastId();

                        foreach ($product_option['product_option_value'] as $product_option_value) {
                            $this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_option_id . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value_id = '" . (int)$product_option_value['option_value_id'] . "', quantity = '" . (int)$product_option_value['quantity'] . "', subtract = '" . (int)$product_option_value['subtract'] . "', price = '" . (float)$product_option_value['price'] . "', price_prefix = '" . $this->db->escape($product_option_value['price_prefix']) . "', points = '" . (int)$product_option_value['points'] . "', points_prefix = '" . $this->db->escape($product_option_value['points_prefix']) . "', weight = '" . (float)$product_option_value['weight'] . "', weight_prefix = '" . $this->db->escape($product_option_value['weight_prefix']) . "'");
                        }
                    }
                } else {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', value = '" . $this->db->escape($product_option['value']) . "', required = '" . (int)$product_option['required'] . "'");
                }
            }
        }
/*
        if (isset($data['product_recurring'])) {
            foreach ($data['product_recurring'] as $recurring) {

                $query = $this->db->query("SELECT `product_id` FROM `" . DB_PREFIX . "product_recurring` WHERE `product_id` = '" . (int)$product_id . "' AND `customer_group_id = '" . (int)$recurring['customer_group_id'] . "' AND `recurring_id` = '" . (int)$recurring['recurring_id'] . "'");

                if (!$query->num_rows) {
                    $this->db->query("INSERT INTO `" . DB_PREFIX . "product_recurring` SET `product_id` = '" . (int)$product_id . "', customer_group_id = '" . (int)$recurring['customer_group_id'] . "', `recurring_id` = '" . (int)$recurring['recurring_id'] . "'");
                }
            }
        }*/

        if (isset($data['product_discount'])) {
            foreach ($data['product_discount'] as $product_discount) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_discount SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_discount['customer_group_id'] . "', quantity = '" . (int)$product_discount['quantity'] . "', priority = '" . (int)$product_discount['priority'] . "', price = '" . (float)$product_discount['price'] . "', date_start = '" . $this->db->escape($product_discount['date_start']) . "', date_end = '" . $this->db->escape($product_discount['date_end']) . "'");
            }
        }

        if (isset($data['product_special'])) {
            foreach ($data['product_special'] as $product_special) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_special['customer_group_id'] . "', priority = '" . (int)$product_special['priority'] . "', price = '" . (float)$product_special['price'] . "', date_start = '" . $this->db->escape($product_special['date_start']) . "', date_end = '" . $this->db->escape($product_special['date_end']) . "'");
            }
        }

        if (isset($data['product_image'])) {
            foreach ($data['product_image'] as $product_image) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape($product_image['image']) . "', sort_order = '" . (int)$product_image['sort_order'] . "'");
            }
        }

        if (isset($data['product_download'])) {
            foreach ($data['product_download'] as $download_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_download SET product_id = '" . (int)$product_id . "', download_id = '" . (int)$download_id . "'");
            }
        }

        if (isset($data['product_category'])) {
            foreach ($data['product_category'] as $category_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
            }
        }

        if (isset($data['product_filter'])) {
            foreach ($data['product_filter'] as $filter_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_filter SET product_id = '" . (int)$product_id . "', filter_id = '" . (int)$filter_id . "'");
            }
        }

        if (isset($data['product_related'])) {
            foreach ($data['product_related'] as $related_id) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "' AND related_id = '" . (int)$related_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$product_id . "', related_id = '" . (int)$related_id . "'");
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$related_id . "' AND related_id = '" . (int)$product_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$related_id . "', related_id = '" . (int)$product_id . "'");
            }
        }

        if (isset($data['product_reward'])) {
            foreach ($data['product_reward'] as $customer_group_id => $product_reward) {
                if ((int)$product_reward['points'] > 0) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_reward SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$customer_group_id . "', points = '" . (int)$product_reward['points'] . "'");
                }
            }
        }

        // SEO URL
        if (isset($data['product_seo_url'])) {
            foreach ($data['product_seo_url'] as $store_id => $language) {
                foreach ($language as $language_id => $keyword) {
                    if (!empty($keyword)) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'product_id=" . (int)$product_id . "', keyword = '" . $this->db->escape($keyword) . "'");
                    }
                }
            }
        }

        if (isset($data['product_layout'])) {
            foreach ($data['product_layout'] as $store_id => $layout_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_layout SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
            }
        }


        $this->cache->delete('product');

        return $product_id;
    }
    public function clearCategories(): void {

        $this->db->query("DELETE FROM " . DB_PREFIX . "category");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_description");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_filter");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_to_store");
        $this->db->query("DELETE FROM " . DB_PREFIX . "category_to_layout");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category");
        $this->db->query("DELETE FROM " . DB_PREFIX . "coupon_category");

        $this->cache->delete('category');
    }
    public function clearProducts(): void {
			$folder = DIR_IMAGE . 'catalog/' . 'VENDER';
		if (file_exists($folder)) {
			rmdir($folder); 
			mkdir($folder, 0755, true);
		}
		else{
			
			mkdir($folder, 0755, true);
		}
        $this->db->query("DELETE FROM " . DB_PREFIX . "product");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_description");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_discount");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_filter");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_image");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_option");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_related");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_reward");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_special");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_download");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_layout");
        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store");
        //$this->db->query("DELETE FROM " . DB_PREFIX . "product_recurring");
        $this->db->query("DELETE FROM " . DB_PREFIX . "review");
        $this->db->query("DELETE FROM " . DB_PREFIX . "seo_url");
        $this->db->query("DELETE FROM " . DB_PREFIX . "coupon_product");

        $this->cache->delete('product');
    }
	
	public function createCronTask($data){
        date_default_timezone_set("Asia/Almaty");
	    $taskCode = rand(1000000, 9999999);
		$addTaskQuery = $this->db->query("INSERT INTO " . DB_PREFIX . "vender_import_cron_data SET data = '" . $data['data'] . "', creation_time = '" . date("Y-m-d H:i:s") . "', execution_time = '" . $data['execution_time'] . "',`task_code` = '".$taskCode."', is_active = '1', clear_catalog = '" . $data['clear_catalog'] . "'");

		return $addTaskQuery;
	}

    public function removeCronTask($taskCode){
        $removeTaskQuery = $this->db->query("DELETE FROM " . DB_PREFIX . "vender_import_cron_data WHERE `task_code` = '".$taskCode."'");

        return $removeTaskQuery;
    }

    public function disableCronTask($taskCode, $enable){

        $enable = $enable == 'true' ? 0 : 1;

        $disableTaskQuery = $this->db->query("UPDATE `" . DB_PREFIX . "vender_import_cron_data` SET `is_active` = '".$enable."' WHERE `oc_vender_import_cron_data`.`task_code` = '".$taskCode."'");

        return $disableTaskQuery;
    }

    public function getCronTasks(){
	    $cronTasks = $this->db->query("SELECT `creation_time`,`execution_time`,`task_code`,`is_active` FROM `" . DB_PREFIX . "vender_import_cron_data`");
        return $cronTasks;
    }
}