<?php
error_reporting(E_ALL);

// Путь к файлу config.php
$configPath = dirname(__DIR__) . '/../../../../config.php';
$db = '';

// Проверка наличия файла config.php
if (!is_file($configPath)) {
    exit('config.php not found');
}

// Подключение файла config.php
require_once($configPath);
// Путь к файлу config.php

// Startup
require_once(DIR_SYSTEM . 'startup.php');

// Autoloader
$autoloader = new \Opencart\System\Engine\Autoloader();
$autoloader->register('Opencart\\Catalog', DIR_APPLICATION);
$autoloader->register('Opencart\Extension', DIR_EXTENSION);
$autoloader->register('Opencart\System', DIR_SYSTEM);

require_once(DIR_SYSTEM . 'vendor.php');

// Registry
$registry = new \Opencart\System\Engine\Registry();
$registry->set('autoloader', $autoloader);

// Config
$config = new \Opencart\System\Engine\Config();
$registry->set('config', $config);

// Load the default config
$config->addPath(DIR_CONFIG);
$config->load('default');
$config->load('catalog');
$config->set('application', 'Catalog');

// Set the default time zone
date_default_timezone_set($config->get('date_timezone'));

// Logging
$log = new \Opencart\System\Library\Log($config->get('error_filename'));
$registry->set('log', $log);

// Error Handler
set_error_handler(function(string $code, string $message, string $file, string $line) use ($log, $config) {
    // error suppressed with @
    if (@error_reporting() === 0) {
        return false;
    }

    switch ($code) {
        case E_NOTICE:
        case E_USER_NOTICE:
            $error = 'Notice';
            break;
        case E_WARNING:
        case E_USER_WARNING:
            $error = 'Warning';
            break;
        case E_ERROR:
        case E_USER_ERROR:
            $error = 'Fatal Error';
            break;
        default:
            $error = 'Unknown';
            break;
    }

    if ($config->get('error_log')) {
        $log->write('PHP ' . $error . ':  ' . $message . ' in ' . $file . ' on line ' . $line);
    }

    if ($config->get('error_display')) {
        echo '<b>' . $error . '</b>: ' . $message . ' in <b>' . $file . '</b> on line <b>' . $line . '</b>';
    } else {
        header('Location: ' . $config->get('error_page'));
        exit();
    }

    return true;
});

// Exception Handler
set_exception_handler(function(\Throwable $e) use ($log, $config)  {
    if ($config->get('error_log')) {
        $log->write(get_class($e) . ':  ' . $e->getMessage() . ' in ' . $e->getFile() . ' on line ' . $e->getLine());
    }

    if ($config->get('error_display')) {
        echo '<b>' . get_class($e) . '</b>: ' . $e->getMessage() . ' in <b>' . $e->getFile() . '</b> on line <b>' . $e->getLine() . '</b>';
    } else {
        header('Location: ' . $config->get('error_page'));
        exit();
    }
});

// Event
$event = new \Opencart\System\Engine\Event($registry);
$registry->set('event', $event);

// Event Register
if ($config->has('action_event')) {
    foreach ($config->get('action_event') as $key => $value) {
        foreach ($value as $priority => $action) {
            $event->register($key, new \Opencart\System\Engine\Action($action), $priority);
        }
    }
}

// Database
if ($config->get('db_autostart')) {
    $db = new \Opencart\System\Library\DB($config->get('db_engine'), $config->get('db_hostname'), $config->get('db_username'), $config->get('db_password'), $config->get('db_database'), $config->get('db_port'));
    $registry->set('db', $db);

    // Sync PHP and DB time zones
    $db->query("SET `time_zone` = '" . $db->escape(date('P')) . "'");
}
date_default_timezone_set("Asia/Almaty");
ExecuteCron();
exit();



function ExecuteCron()
{

    $tasks = getCronTasks();
    if($tasks->num_rows > 0) {
        import(json_decode($tasks->rows[0]['data'], false));
    }


}

function import($data)
{
    global $registry;
    ini_set('max_execution_time', 0);
    set_time_limit(0);
    $loader = $registry->get('load');
    $db = $registry->get('db');
    $log = $registry->get('log');

	$folder = DIR_IMAGE . 'catalog/' . 'VENDER';
	if (file_exists($folder)) {
		rmdir($folder); 
		mkdir($folder, 0755, true);
	}
	
	else{
		
		mkdir($folder, 0755, true);
	}
    clearProducts();
    clearCategories();

    addTopCategory();

    foreach ($data as $catID => $catData) {
        $importData = [
            'vender_cat_id' => $catID,
            'category_description' => [
                1 => [
                    'name' => $catData->catName,
                    'description' => $catData->catName,
                    'meta_title' => $catData->catName,
                    'meta_description' => "",
                    'meta_keyword' => "",
                ],
            ],
            'category_store' => [
                0 => 0
            ],
            'top' => $catData->toMenu == 1 ? 1 : 0,
            'path' => $catData->catName,
            'parent_id' => 1,
            'filter' => "",
            'image' => "",
            'column' => "",
            'sort_order' => "",
            'status' => 1,
        ];

        $category_id = addCategory($importData);

        for ($i = 0; $i < count($catData->children); $i++) {
            $importData = [
                'vender_cat_id' => $catID,
                'category_description' => [
                    1 => [
                        'name' => $catData->children[$i]->catName,
                        'description' => $catData->children[$i]->catName,
                        'meta_title' => $catData->children[$i]->catName,
                        'meta_description' => "",
                        'meta_keyword' => "",
                    ],
                ],
                'category_store' => [
                    0 => 0
                ],
                'top' => 1,
                'path' => $catData->catName,
                'parent_id' => $category_id,
                'filter' => "",
                'image' => "",
                'column' => "",
                'sort_order' => "",
                'status' => 1,
            ];

            $currentCat = addCategory($importData);
            importProducts($catData->children[$i]->catid, $currentCat);
        }
    }
    $db->query("UPDATE `" . DB_PREFIX . "vender_import_cron_data` SET `last_end_time` = '".date("Y-m-d H:i:s")."' WHERE `oc_vender_import_cron_data`.`task_code` = '".$taskCode."'");
	

}

function importProducts($catId, $currentCat){


    ini_set('max_execution_time', 0);
    set_time_limit(0);

    //Получение API_KEY из настроек
    $module_id = getModuleCode();
    $module_info = getModule($module_id[0]['module_id']);


    $URL = "https://api.al-style.kz/api/elements?access-token=".$module_info->api_key."&category=";
    $content = json_decode(file_get_contents($URL . $catId . '&additional_fields=images,description,brand,weight'));
    $folder = DIR_IMAGE . 'catalog/' . 'VENDER';

    foreach ($content as $item) {
        $elementDataURL = "https://api.al-style.kz/api/element-info?access-token=".$module_info->api_key."&article=";
        $element_info = json_decode(file_get_contents($elementDataURL . $item->article . '&additional_fields=detailtext'))[0];

        $mainImage = '';
        $images = [];

        if (!is_dir($folder)) {
            mkdir($folder, 0777, true);
            chmod($folder, 0777);
            @touch($folder . 'index.html');
        }

        for ($i = 0; $i < count($item->images); $i++) {
            $extension = pathinfo(basename($item->images[$i]), PATHINFO_EXTENSION);
            $filename = md5(microtime()) . '.' . $extension;
            $i === 0 ? $mainImage = 'catalog/' . 'VENDER/' . $filename : '';

            if (file_put_contents($folder . '/' . $filename, file_get_contents($item->images[$i]))) {
                $images[$i] = [
                    'image' => 'catalog/' . 'VENDER/' . $filename,
                    'sort_order' => $i
                ];
            }
        }

        $productData = [
            'product_description' => [
                1 => [
                    'name' => $item->name,
                    'description' => $element_info->detailtext,
                    'meta_title' => $item->name,
                    'meta_description' => $item->full_name,
                    'meta_keyword' => '',
                    'tag' => '',
                ],
            ],
            'model' => $item->name,
            'sku' => $item->article,
            'upc' => '',
            'ean' => '',
            'jan' => '',
            'isbn' => '',
            'mpn' => '',
            'location' => '',
            'price' => $item->price2,
            'tax_class_id' => 9,
            'quantity' => $item->quantity,
            'minimum' => 1,
            'subtract' => 1,
            'stock_status_id' => $item->quantity > 0 ? 7 : 5,
            'shipping' => 1,
            'date_available' => date('Y-m-d'),
            'length' => 0,
            'width' => 0,
            'height' => 1,
            'length_class_id' => 0,
            'weight' => $item->weight,
            'weight_class_id' => 0,
            'status' => $item->quantity > 0 ? 1 : 0,
            'sort_order' => 0,
            'manufacturer' => $item->brand,
            'manufacturer_id' => 0,
            'category' => '',
            'product_category' => [
                0 => $currentCat
            ],
            'filter' => '',
            'product_store' => [
                0 => 0
            ],
            'download' => '',
            'related' => '',
            'product_related' => [
                0 => 42,
                1 => 30,
            ],
            'option' => '',
            'image' => $mainImage,
            'product_image' => $images,
            'points' => '',
            'product_reward' => [
                1 => [
                    'points' => '',
                ],
            ],
            'product_seo_url' => [
                0 => [
                    1 => translit_sef($item->name),
                ],
            ],
            'product_layout' => [
                0 => '',
            ],
        ];

        addProduct($productData);
    }

}

function addTopCategory(): bool {
    global $registry;
    $db = $registry->get('db');

    $db->query("INSERT INTO " . DB_PREFIX . "category SET category_id = '0',  parent_id = '0', `top` = '1',status = '1', date_modified = NOW(), date_added = NOW()");
    $category_id = $db->getLastId();
    $db->query("UPDATE `" . DB_PREFIX . "category` SET `category_id` = '1' WHERE `oc_category`.`category_id` = ".$category_id.";");
    $db->query("INSERT INTO " . DB_PREFIX . "category_description SET category_id = '1', language_id = '1', name = 'Каталог', description = 'Каталог', meta_title = 'Каталог', meta_description = 'Каталог', meta_keyword = 'Каталог'");
    $db->query("INSERT INTO " . DB_PREFIX . "category_to_store SET category_id = '1', store_id = '0'");
    $db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '0', language_id = '1', query = 'category_id=0'");
    $db->query("INSERT INTO " . DB_PREFIX . "category_to_layout SET category_id = '1', store_id = '0', layout_id = '1'");


    return true;
}
function addCategory($data): float {

    global $registry;
    $db = $registry->get('db');

    $db->query("INSERT INTO " . DB_PREFIX . "category SET parent_id = '" . (int)$data['parent_id'] . "', vender_cat_id = '" . (int)$data['vender_cat_id'] . "', `top` = '" . (isset($data['top']) ? (int)$data['top'] : 0) . "', `column` = '" . (int)$data['column'] . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', date_modified = NOW(), date_added = NOW()");

    $category_id = $db->getLastId();

    if (isset($data['image'])) {
        $db->query("UPDATE " . DB_PREFIX . "category SET image = '" . $db->escape($data['image']) . "'");
    }

    foreach ($data['category_description'] as $language_id => $value) {
        $db->query("INSERT INTO " . DB_PREFIX . "category_description SET category_id = '" . (int)$category_id . "', language_id = '" . (int)$language_id . "', name = '" . $db->escape($value['name']) . "', description = '" . $db->escape($value['description']) . "', meta_title = '" . $db->escape($value['meta_title']) . "', meta_description = '" . $db->escape($value['meta_description']) . "', meta_keyword = '" . $db->escape($value['meta_keyword']) . "'");
    }

    // MySQL Hierarchical Data Closure Table Pattern
    $level = 0;

    $query = $db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$data['parent_id'] . "' ORDER BY `level` ASC");

    foreach ($query->rows as $result) {
        $db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$result['path_id'] . "', `level` = '" . (int)$level . "'");

        $level++;
    }

    $db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$category_id . "', `level` = '" . (int)$level . "'");

    if (isset($data['category_filter'])) {
        foreach ($data['category_filter'] as $filter_id) {
            $db->query("INSERT INTO " . DB_PREFIX . "category_filter SET category_id = '" . (int)$category_id . "', filter_id = '" . (int)$filter_id . "'");
        }
    }

    if (isset($data['category_store'])) {
        foreach ($data['category_store'] as $store_id) {
            $db->query("INSERT INTO " . DB_PREFIX . "category_to_store SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "'");
        }
    }

    if (isset($data['category_seo_url'])) {
        foreach ($data['category_seo_url'] as $store_id => $language) {
            foreach ($language as $language_id => $keyword) {
                if (!empty($keyword)) {
                    $db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'category_id=" . (int)$category_id . "', keyword = '" . $db->escape($keyword) . "'");

                }
            }
        }
    }

    // Set which layout to use with this category
    if (isset($data['category_layout'])) {
        foreach ($data['category_layout'] as $store_id => $layout_id) {
            $db->query("INSERT INTO " . DB_PREFIX . "category_to_layout SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
        }
    }


    return $category_id;
}
function addProduct($data): float {

    global $registry;
    $db = $registry->get('db');
    $db->query("INSERT INTO " . DB_PREFIX . "product SET model = '" . $db->escape($data['model']) . "', sku = '" . $db->escape($data['sku']) . "', upc = '" . $db->escape($data['upc']) . "', ean = '" . $db->escape($data['ean']) . "', jan = '" . $db->escape($data['jan']) . "', isbn = '" . $db->escape($data['isbn']) . "', mpn = '" . $db->escape($data['mpn']) . "', location = '" . $db->escape($data['location']) . "', quantity = '" . (int)$data['quantity'] . "', minimum = '" . (int)$data['minimum'] . "', subtract = '" . (int)$data['subtract'] . "', stock_status_id = '" . (int)$data['stock_status_id'] . "', date_available = '" . $db->escape($data['date_available']) . "', manufacturer_id = '" . (int)$data['manufacturer_id'] . "', shipping = '" . (int)$data['shipping'] . "', price = '" . (float)$data['price'] . "', points = '" . (int)$data['points'] . "', weight = '" . (float)$data['weight'] . "', weight_class_id = '" . (int)$data['weight_class_id'] . "', length = '" . (float)$data['length'] . "', width = '" . (float)$data['width'] . "', height = '" . (float)$data['height'] . "', length_class_id = '" . (int)$data['length_class_id'] . "', status = '" . (int)$data['status'] . "', tax_class_id = '" . (int)$data['tax_class_id'] . "', sort_order = '" . (int)$data['sort_order'] . "', date_added = NOW(), date_modified = NOW()");

    $product_id = $db->getLastId();

    if (isset($data['image'])) {
        $db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $db->escape($data['image']) . "' WHERE product_id = '" . (int)$product_id . "'");
    }

    foreach ($data['product_description'] as $language_id => $value) {
        $db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$language_id . "', name = '" . $db->escape($value['name']) . "', description = '" . $db->escape($value['description']) . "', tag = '" . $db->escape($value['tag']) . "', meta_title = '" . $db->escape($value['meta_title']) . "', meta_description = '" . $db->escape($value['meta_description']) . "', meta_keyword = '" . $db->escape($value['meta_keyword']) . "'");
    }

    if (isset($data['product_store'])) {
        foreach ($data['product_store'] as $store_id) {
            $db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "'");
        }
    }

    if (isset($data['product_attribute'])) {
        foreach ($data['product_attribute'] as $product_attribute) {
            if ($product_attribute['attribute_id']) {
                // Removes duplicates
                $db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "'");

                foreach ($product_attribute['product_attribute_description'] as $language_id => $product_attribute_description) {
                    $db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "' AND language_id = '" . (int)$language_id . "'");

                    $db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int)$product_id . "', attribute_id = '" . (int)$product_attribute['attribute_id'] . "', language_id = '" . (int)$language_id . "', text = '" .  $db->escape($product_attribute_description['text']) . "'");
                }
            }
        }
    }

    if (isset($data['product_option'])) {
        foreach ($data['product_option'] as $product_option) {
            if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
                if (isset($product_option['product_option_value'])) {
                    $db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', required = '" . (int)$product_option['required'] . "'");

                    $product_option_id = $db->getLastId();

                    foreach ($product_option['product_option_value'] as $product_option_value) {
                        $db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_option_id . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value_id = '" . (int)$product_option_value['option_value_id'] . "', quantity = '" . (int)$product_option_value['quantity'] . "', subtract = '" . (int)$product_option_value['subtract'] . "', price = '" . (float)$product_option_value['price'] . "', price_prefix = '" . $db->escape($product_option_value['price_prefix']) . "', points = '" . (int)$product_option_value['points'] . "', points_prefix = '" . $db->escape($product_option_value['points_prefix']) . "', weight = '" . (float)$product_option_value['weight'] . "', weight_prefix = '" . $db->escape($product_option_value['weight_prefix']) . "'");
                    }
                }
            } else {
                $db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', value = '" . $db->escape($product_option['value']) . "', required = '" . (int)$product_option['required'] . "'");
            }
        }
    }
    /*
            if (isset($data['product_recurring'])) {
                foreach ($data['product_recurring'] as $recurring) {

                    $query = $db->query("SELECT `product_id` FROM `" . DB_PREFIX . "product_recurring` WHERE `product_id` = '" . (int)$product_id . "' AND `customer_group_id = '" . (int)$recurring['customer_group_id'] . "' AND `recurring_id` = '" . (int)$recurring['recurring_id'] . "'");

                    if (!$query->num_rows) {
                        $db->query("INSERT INTO `" . DB_PREFIX . "product_recurring` SET `product_id` = '" . (int)$product_id . "', customer_group_id = '" . (int)$recurring['customer_group_id'] . "', `recurring_id` = '" . (int)$recurring['recurring_id'] . "'");
                    }
                }
            }*/

    if (isset($data['product_discount'])) {
        foreach ($data['product_discount'] as $product_discount) {
            $db->query("INSERT INTO " . DB_PREFIX . "product_discount SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_discount['customer_group_id'] . "', quantity = '" . (int)$product_discount['quantity'] . "', priority = '" . (int)$product_discount['priority'] . "', price = '" . (float)$product_discount['price'] . "', date_start = '" . $db->escape($product_discount['date_start']) . "', date_end = '" . $db->escape($product_discount['date_end']) . "'");
        }
    }

    if (isset($data['product_special'])) {
        foreach ($data['product_special'] as $product_special) {
            $db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_special['customer_group_id'] . "', priority = '" . (int)$product_special['priority'] . "', price = '" . (float)$product_special['price'] . "', date_start = '" . $db->escape($product_special['date_start']) . "', date_end = '" . $db->escape($product_special['date_end']) . "'");
        }
    }

    if (isset($data['product_image'])) {
        foreach ($data['product_image'] as $product_image) {
            $db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $db->escape($product_image['image']) . "', sort_order = '" . (int)$product_image['sort_order'] . "'");
        }
    }

    if (isset($data['product_download'])) {
        foreach ($data['product_download'] as $download_id) {
            $db->query("INSERT INTO " . DB_PREFIX . "product_to_download SET product_id = '" . (int)$product_id . "', download_id = '" . (int)$download_id . "'");
        }
    }

    if (isset($data['product_category'])) {
        foreach ($data['product_category'] as $category_id) {
            $db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
        }
    }

    if (isset($data['product_filter'])) {
        foreach ($data['product_filter'] as $filter_id) {
            $db->query("INSERT INTO " . DB_PREFIX . "product_filter SET product_id = '" . (int)$product_id . "', filter_id = '" . (int)$filter_id . "'");
        }
    }

    if (isset($data['product_related'])) {
        foreach ($data['product_related'] as $related_id) {
            $db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "' AND related_id = '" . (int)$related_id . "'");
            $db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$product_id . "', related_id = '" . (int)$related_id . "'");
            $db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$related_id . "' AND related_id = '" . (int)$product_id . "'");
            $db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$related_id . "', related_id = '" . (int)$product_id . "'");
        }
    }

    if (isset($data['product_reward'])) {
        foreach ($data['product_reward'] as $customer_group_id => $product_reward) {
            if ((int)$product_reward['points'] > 0) {
                $db->query("INSERT INTO " . DB_PREFIX . "product_reward SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$customer_group_id . "', points = '" . (int)$product_reward['points'] . "'");
            }
        }
    }

    // SEO URL
    if (isset($data['product_seo_url'])) {
        foreach ($data['product_seo_url'] as $store_id => $language) {
            foreach ($language as $language_id => $keyword) {
                if (!empty($keyword)) {
                    $db->query("INSERT INTO " . DB_PREFIX . "seo_url SET store_id = '" . (int)$store_id . "', language_id = '" . (int)$language_id . "', query = 'product_id=" . (int)$product_id . "', keyword = '" . $db->escape($keyword) . "'");
                }
            }
        }
    }

    if (isset($data['product_layout'])) {
        foreach ($data['product_layout'] as $store_id => $layout_id) {
            $db->query("INSERT INTO " . DB_PREFIX . "product_to_layout SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
        }
    }

    return $product_id;
}
function clearCategories(): void {

    global $registry;
    $db = $registry->get('db');

    $db->query("DELETE FROM " . DB_PREFIX . "category");
    $db->query("DELETE FROM " . DB_PREFIX . "category_description");
    $db->query("DELETE FROM " . DB_PREFIX . "category_filter");
    $db->query("DELETE FROM " . DB_PREFIX . "category_to_store");
    $db->query("DELETE FROM " . DB_PREFIX . "category_to_layout");
    $db->query("DELETE FROM " . DB_PREFIX . "product_to_category");
    $db->query("DELETE FROM " . DB_PREFIX . "coupon_category");

}
function clearProducts(): void {

    global $registry;
    $db = $registry->get('db');

    $db->query("DELETE FROM " . DB_PREFIX . "product");
    $db->query("DELETE FROM " . DB_PREFIX . "product_attribute");
    $db->query("DELETE FROM " . DB_PREFIX . "product_description");
    $db->query("DELETE FROM " . DB_PREFIX . "product_discount");
    $db->query("DELETE FROM " . DB_PREFIX . "product_filter");
    $db->query("DELETE FROM " . DB_PREFIX . "product_image");
    $db->query("DELETE FROM " . DB_PREFIX . "product_option");
    $db->query("DELETE FROM " . DB_PREFIX . "product_option_value");
    $db->query("DELETE FROM " . DB_PREFIX . "product_related");
    $db->query("DELETE FROM " . DB_PREFIX . "product_reward");
    $db->query("DELETE FROM " . DB_PREFIX . "product_special");
    $db->query("DELETE FROM " . DB_PREFIX . "product_to_category");
    $db->query("DELETE FROM " . DB_PREFIX . "product_to_download");
    $db->query("DELETE FROM " . DB_PREFIX . "product_to_layout");
    $db->query("DELETE FROM " . DB_PREFIX . "product_to_store");
    //$db->query("DELETE FROM " . DB_PREFIX . "product_recurring");
    $db->query("DELETE FROM " . DB_PREFIX . "review");
    $db->query("DELETE FROM " . DB_PREFIX . "seo_url");
    $db->query("DELETE FROM " . DB_PREFIX . "coupon_product");

}
function getModuleCode(){
    global $registry;
    $db = $registry->get('db');
    return  $db->query("SELECT `module_id` FROM `" . DB_PREFIX . "module` WHERE `name` = 'VenderImport'")->rows;
}
function getModule($moduleID){
    global $registry;
    $db = $registry->get('db');
    return json_decode($db->query("SELECT `setting` FROM `" . DB_PREFIX . "module` WHERE `module_id` = '".$moduleID."';")->rows[0]['setting']);
}

function translit_sef($value)
{
    $converter = array(
        'а' => 'a',    'б' => 'b',    'в' => 'v',    'г' => 'g',    'д' => 'd',
        'е' => 'e',    'ё' => 'e',    'ж' => 'zh',   'з' => 'z',    'и' => 'i',
        'й' => 'y',    'к' => 'k',    'л' => 'l',    'м' => 'm',    'н' => 'n',
        'о' => 'o',    'п' => 'p',    'р' => 'r',    'с' => 's',    'т' => 't',
        'у' => 'u',    'ф' => 'f',    'х' => 'h',    'ц' => 'c',    'ч' => 'ch',
        'ш' => 'sh',   'щ' => 'sch',  'ь' => '',     'ы' => 'y',    'ъ' => '',
        'э' => 'e',    'ю' => 'yu',   'я' => 'ya',
    );

    $value = mb_strtolower($value);
    $value = strtr($value, $converter);
    $value = mb_ereg_replace('[^-0-9a-z]', '-', $value);
    $value = mb_ereg_replace('[-]+', '-', $value);
    $value = trim($value, '-');

    return $value;
}
function clearFolder($folder) {
	if ($objs = glob($folder . '/*')) {
		foreach($objs as $obj) {
			is_dir($obj) ? clear_dir($obj, true) : unlink($obj);
		}
	}

}

function getCronTasks(){
    global $registry;
    $db = $registry->get('db');
    $res = $db->query("SELECT * FROM `oc_vender_import_cron_data` WHERE `execution_time` ='".date("H:00")." AND `is_active` = 1';");
    return $res;
}

