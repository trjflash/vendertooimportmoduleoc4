<?php
// Heading
$_['heading_title']    = 'Получение товаров от ТОО Vender';

// Text
$_['text_extension']    = 'Расширения';
$_['text_success']      = 'Настройки успешно изменены!';
$_['text_edit']         = 'Настройки модуля';

// Entry
$_['entry_name']        = 'Компания';
$_['entry_title']       = 'Заголовок';
$_['entry_description'] = 'Текст';
$_['entry_status']      = 'Статус';

//API settings
$_['api_key_label']           = 'Ваш API KEY';

//Visual

$_['importSettingsTabText']     ='Настройки импорта';
$_['cronDataTabText']     ='Список заданий CRON';

$_['CronCreationTime']     ='Время создания';
$_['CronLastRunTime']     ='Время запуска';
$_['CronLastEndTime']     ='Время завершения';
$_['CronTaskKey']     ='Ключ';
$_['CronTaskStatus']     ='Статус';
$_['CronTaskAction']     ='Действие';

$_['load_categories_label']     ='Категории для импорта';
$_['load_categories_btn']       ='Загрузить доступные категории';

$_['import_btn']       ='Импортировать выбранные категории сейчас';
$_['create_cron_btn']  ='Создать задачу для импорта';

// Error
$_['error_permission']  = 'У Вас нет прав для управления данным модулем!';
$_['error_name']        = 'Название модуля должно содержать от 3 до 64 символов!';


$_['cron_task_created']      = 'Задание для импорта успешно созданно!';
$_['cron_task_error']        = 'Не удалосб создать задание для импорта';

$_['cron_task_edit_error']        = 'Не удалось изменить задание для импорта';
$_['cron_task_edited']        = 'Задание отключенно';
$_['cron_task_removed']        = 'Задание удаленно';

$_['empty_import_data']        = 'Выберите категории для импорта';

