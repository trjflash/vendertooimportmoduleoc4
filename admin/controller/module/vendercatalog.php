<?php
namespace Opencart\Admin\Controller\Extension\VenderCatalog\module;

class VenderCatalog extends \Opencart\System\Engine\Controller {
   
	
    private $error = array();
	private $lastRequestTime = null;

	public function install()
    {
        $this->load->model('extension/VenderCatalog/module/vendercatalog');

        $this->model_extension_VenderCatalog_module_vendercatalog->createTable();
        $this->model_extension_VenderCatalog_module_vendercatalog->addRows();
    }

	public function uninstall()
    {
        // Дополнительные действия при удалении модуля

        $this->load->model('extension/VenderCatalog/module/vendercatalog');
        $this->model_extension_VenderCatalog_module_vendercatalog->dropTable();

        // Продолжение стандартного удаления модуля
        $this->load->model('setting/setting');
        $this->model_setting_setting->deleteSetting('vendercatalog');

        $this->load->model('extension/modification');
        $this->model_extension_modification->deleteModificationByCode('vendercatalog');

        $this->load->model('user/user_group');
        $this->model_user_user_group->removePermission($this->user->getGroupId(), 'access', 'extension/module/vendercatalog');
        $this->model_user_user_group->removePermission($this->user->getGroupId(), 'modify', 'extension/module/vendercatalog');
    }
	
	public function index(): void {
				
        $this->load->model('extension/VenderCatalog/module/vendercatalog');
		$this->load->language('extension/VenderCatalog/module/vendercatalog');

		$this->document->setTitle($this->language->get('heading_title'));
		
        $this->load->model('setting/module');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            if (!isset($this->request->get['module_id'])) {
                $this->model_setting_module->addModule('vendercatalog', $this->request->post);
            } else {
                $this->model_setting_module->editModule($this->request->get['module_id'], $this->request->post);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', 'SSL'));
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_edit'] = $this->language->get('text_edit');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');

        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_status'] = $this->language->get('entry_status');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['name'])) {
            $data['error_name'] = $this->error['name'];
        } else {
            $data['error_name'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_extension'),
            'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/VenderCatalog/module/vendercatalog', 'user_token=' . $this->session->data['user_token'], true)
        );

		$module_id = $this->model_extension_VenderCatalog_module_vendercatalog->getModuleCode();
					
		if(isset($this->request->get['module_id'])){
			$data['module_id'] = (int)$this->request->get['module_id'];
			$module_info = $this->model_setting_module->getModule($this->request->get['module_id']);
		}
		else if(isset($module_id[0]['module_id'])){
			$data['module_id'] = $module_id[0]['module_id'];
			$module_info = $this->model_setting_module->getModule($data['module_id']);
		}
		else{
			$data['module_id'] = 0;
			$module_info = null;
		}


		if (!isset($this->request->get['module_id'])) {
			$data['breadcrumbs'][] = [
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('extension/VenderCatalog/module/vendercatalog', 'user_token=' . $this->session->data['user_token'])
			];
		} else {
			$data['breadcrumbs'][] = [
				'text' => $this->language->get('heading_title'),
				'href' => $this->url->link('extension/VenderCatalog/module/vendercatalog', 'user_token=' . $this->session->data['user_token'] . '&module_id=' . $this->request->get['module_id'])
			];
		}

        $cron_tasks = $this->model_extension_VenderCatalog_module_vendercatalog->getCronTasks();


        if($cron_tasks->num_rows > 0){
            $data['cronTasksList'] = $cron_tasks->rows;

            $data['cronTasksFile'] = dirname(__DIR__, 2).'/model/module/cronjob.php';
        }

		
		$data['save'] = $this->url->link('extension/VenderCatalog/module/vendercatalog.save', 'user_token=' . $this->session->data['user_token'] . (isset($this->request->get['module_id']) ? '&module_id=' . $this->request->get['module_id'] : ''));
		$data['back'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module');
		$this->load->model('setting/module');
				
		$data['api_key'] = isset($module_info['api_key']) ? $module_info['api_key'] : "";
		
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
		
			
        $this->response->setOutput($this->load->view('extension/VenderCatalog/module/vendercatalog', $data));
    }

    protected function validate() {
		ini_set('max_execution_time', 0);
		set_time_limit(0);
		/*
        echo "<pre>";
        print_r($this->request->post);
        exit();
		echo "</pre>";*/
		
        if (!$this->user->hasPermission('modify', 'extension/VenderCatalog/module/vendercatalog')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (isset($this->request->post['api_key']) && ((strlen($this->request->post['api_key']) < 3) || (strlen($this->request->post['api_key']) > 64))) {
            $this->error['api_key'] = $this->language->get('error_name');
        }

        return !$this->error;
    }

    public function import($direct = false, $directData="")
	{
		ini_set('max_execution_time', 0);
		set_time_limit(0);
		//$data = json_decode($data);
		$this->load->model('extension/VenderCatalog/module/vendercatalog');

		if (!$direct) {
			$data = json_decode($_POST['data'], false);
		} else {
			$data = $directData;
		}


		if ($_POST['clear']) {
			$folder = DIR_IMAGE . 'catalog/' . 'VENDER';
			$this->clearFolder($folder);

			$this->model_extension_VenderCatalog_module_vendercatalog->clearProducts();
			$this->model_extension_VenderCatalog_module_vendercatalog->clearCategories();
			
			$this->model_extension_VenderCatalog_module_vendercatalog->addTopCategory();

			foreach ($data as $catID => $catData) {
				$importData = [
					'vender_cat_id' => $catID,
					'category_description' => [
						1 => [
							'name' => $catData->catName,
							'description' => $catData->catName,
							'meta_title' => $catData->catName,
							'meta_description' => "",
							'meta_keyword' => "",
						],
					],
					'category_store' => [
						0 => 0
					],
					'top' => $catData->toMenu == 1 ? 1 : 0,
					'path' => $catData->catName,
					'parent_id' => 1,
					'filter' => "",
					'image' => "",
					'column' => "",
					'sort_order' => "",
					'status' => 1,
				];

				$category_id = $this->model_extension_VenderCatalog_module_vendercatalog->addCategory($importData);

				for ($i = 0; $i < count($catData->children); $i++) {
					$importData = [
						'vender_cat_id' => $catID,
						'category_description' => [
							1 => [
								'name' => $catData->children[$i]->catName,
								'description' => $catData->children[$i]->catName,
								'meta_title' => $catData->children[$i]->catName,
								'meta_description' => "",
								'meta_keyword' => "",
							],
						],
						'category_store' => [
							0 => 0
						],
						'top' => 1,
						'path' => $catData->catName,
						'parent_id' => $category_id,
						'filter' => "",
						'image' => "",
						'column' => "",
						'sort_order' => "",
						'status' => 1,
					];

					$currentCat = $this->model_extension_VenderCatalog_module_vendercatalog->addCategory($importData);
					$this->importProducts($catData->children[$i]->catid, $currentCat);
				}
			}
		}

		echo json_encode('success');
	}
	
	private function ensureDelay() {
        if ($this->lastRequestTime !== null) {
            $elapsedTime = time() - $this->lastRequestTime;
            $delay = 5 - $elapsedTime;

            if ($delay > 0) {
                sleep($delay); // Задержка на оставшееся время
            }
        }
    }
	private function fetchDataFromAPI($url, $asArray = false)
	{
		$this->ensureDelay();

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_FAILONERROR, true); // Обработка ошибок HTTP статусов (например, 403)
		curl_setopt($ch, CURLOPT_USERAGENT, 'OpenCartVenderAPI/1.0');

		// Выполнение запроса
		$response = curl_exec($ch);

		// Проверка на ошибки
		if ($response === false) {
			$error_msg = curl_error($ch);
			echo "Ошибка при получении данных: " . $error_msg;
			curl_close($ch);
			exit();
		}

		// Получение HTTP-кода ответа
		$http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);

		if ($http_code != 200) {
			echo "Ошибка: сервер вернул статус: " . $http_code;
			exit();
		}

		$this->lastRequestTime = time();

		return json_decode($response, $asArray);
	}
	
	private function printAndStop($array)
	{
		echo("</pre>");
		print_r($array);
		echo("</pre>");
		exit();
	}
	
	private function importProducts($catId, $currentCat)
	{
		ini_set('max_execution_time', 0);
		set_time_limit(0);
		$this->load->model('setting/module');
		$this->load->model('extension/VenderCatalog/module/vendercatalog');
		
		// Получение API_KEY из настроек
		$module_id = $this->model_extension_VenderCatalog_module_vendercatalog->getModuleCode();            
		$module_info = $this->model_setting_module->getModule($module_id[0]['module_id']);

		$this->ensureDelay();
		
		$URL = "https://api.al-style.kz/api/elements?access-token=" . $module_info['api_key'] . "&category=". $catId . '';
		$content = $this->fetchDataFromAPI($URL);
		
		$this->lastRequestTime = time();
		
		$folder = DIR_IMAGE . 'catalog/' . 'VENDER';

		// Сбор артикулов в одну строку
		$articles = [];
		foreach ($content as $item) {
			$articles[] = $item->article;
		}
		
		$articleList = implode(',', $articles);

		// Получение данных по всем артикулам за один запрос
		$elementDataURL = "https://api.al-style.kz/api/element-info?access-token=" . $module_info['api_key'] . "&article=" . $articleList . '&additional_fields=detailText,images,description,brand,weight';
		$elements_info = $this->fetchDataFromAPI($elementDataURL);
		
		// Проход по каждому элементу и его обработка
		
		
		foreach ($elements_info as $item) {
			$mainImage = '';
			$images = [];

			if (!is_dir($folder)) {
				mkdir($folder, 0777, true);
				chmod($folder, 0777);
				@touch($folder . 'index.html');
			}

			// Обработка изображений и создание товара
			for ($i = 0; $i < count($item->images); $i++) {
				$extension = pathinfo(basename($item->images[$i]), PATHINFO_EXTENSION);
				$filename = md5(microtime()) . '.' . $extension;
				$i === 0 ? $mainImage = 'catalog/' . 'VENDER/' . $filename : '';

				$imagePath = $item->images[$i];

				// Определение максимальной ширины и высоты для сжатия
				$maxWidth = 800;
				$maxHeight = 600;

				// Получение размеров изображения
				list($width, $height) = getimagesize($imagePath);

				// Определение новых размеров с учётом пропорций
				$ratio = min($maxWidth / $width, $maxHeight / $height);
				$newWidth = $width * $ratio;
				$newHeight = $height * $ratio;

				// Создание нового изображения с использованием GD
				$compressedImage = imagecreatetruecolor($newWidth, $newHeight);
				$image = imagecreatefromjpeg($imagePath); // Измените функцию в зависимости от типа файла

				// Сжатие изображения
				imagecopyresampled($compressedImage, $image, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);

				// Путь и имя для сжатого изображения
				$compressedImagePath = $folder . '/' . $filename;

				// Сохранение сжатого изображения
				imagejpeg($compressedImage, $compressedImagePath); // Измените функцию в зависимости от типа файла

				// Очистка памяти от временных изображений
				imagedestroy($image);
				imagedestroy($compressedImage);

				// Обновление информации о изображении в массиве
				if (file_exists($compressedImagePath)) {
					$images[$i] = [
						'image' => 'catalog/' . 'VENDER/' . $filename,
						'sort_order' => $i
					];
				}
			}
			// Создание массива данных для продукта
			$productData = [
				'product_description' => [
					1 => [
						'name' => $item->name,
						'description' => $item->detailText,
						'meta_title' => $item->name,
						'meta_description' => $item->full_name,
						'meta_keyword' => '',
						'tag' => '',
					],
				],
				'model' => $item->name,
				'sku' => $item->article,
				'upc' => '',
				'ean' => '',
				'jan' => '',
				'isbn' => '',
				'mpn' => '',
				'location' => '',
				'price' => $item->price2,
				'tax_class_id' => 9,
				'quantity' => $item->quantity,
				'minimum' => 1,
				'subtract' => 1,
				'stock_status_id' => $item->quantity > 0 ? 7 : 5,
				'shipping' => 1,
				'date_available' => date('Y-m-d'),
				'length' => 0,
				'width' => 0,
				'height' => 1,
				'length_class_id' => 0,
				'weight' => $item->weight,
				'weight_class_id' => 0,
				'status' => $item->quantity > 0 ? 1 : 0,
				'sort_order' => 0,
				'manufacturer' => $item->brand,
				'manufacturer_id' => 0,
				'category' => '',
				'product_category' => [
					0 => $currentCat
				],
				'filter' => '',
				'product_store' => [
					0 => 0
				],
				'download' => '',
				'related' => '',
				'product_related' => [
					0 => 42,
					1 => 30,
				],
				'option' => '',
				'image' => $mainImage,
				'product_image' => $images,
				'points' => '',
				'product_reward' => [
					1 => [
						'points' => '',
					],
				],
				'product_seo_url' => [
					0 => [
						1 => $this->translit_sef($item->name),
					],
				],
				'product_layout' => [
					0 => '',
				],
			];

			// Добавление продукта
			$this->model_extension_VenderCatalog_module_vendercatalog->addProduct($productData);
		}
	}

	public function CreateCronTask(){

		$this->load->language('extension/VenderCatalog/module/vendercatalog');
		$this->load->model('extension/VenderCatalog/module/vendercatalog');
		
		$cronData['data'] = $_POST['data'];
		if($cronData['data'] === '{}'){
	
			$json['error'] = $this->language->get('empty_import_data');
			$json['result'] = 'error';
			$this->response->setOutput(json_encode($json));
		}
		
		$cronData['execution_time'] = $_POST['cronTime'];
		$cronData['clear_catalog'] = $_POST['clear'] ? 1 : 0;
		

		
		$result = $this->model_extension_VenderCatalog_module_vendercatalog->createCronTask($cronData);
		
		if($result){
			$json['success'] = $this->language->get('cron_task_created');
			$json['result'] = 'success';
			$this->response->setOutput(json_encode($json));
		}
		else{
			$json['error'] = $this->language->get('cron_task_error');
			$json['result'] = 'error';
			$this->response->setOutput(json_encode($json));
		}

	}

	public function DisableCronTask()
    {
        $this->load->language('extension/VenderCatalog/module/vendercatalog');
        $this->load->model('extension/VenderCatalog/module/vendercatalog');

        $taskId = $_POST['taskId'];
        if($taskId === '{}'){

            $json['error'] = $this->language->get('empty_import_data');
            $json['result'] = 'error';
            $this->response->setOutput(json_encode($json));
        }

        $result = $this->model_extension_VenderCatalog_module_vendercatalog->disableCronTask($taskId, $_POST['enable']);

        if($result){
            $json['success'] = $this->language->get('cron_task_edited');
            $json['result'] = 'success';
            $this->response->setOutput(json_encode($json));
        }
        else{
            $json['error'] = $this->language->get('cron_task_edit_error');
            $json['result'] = 'error';
            $this->response->setOutput(json_encode($json));
        }
    }


    public function RemoveCronTask()
    {
        $this->load->language('extension/VenderCatalog/module/vendercatalog');
        $this->load->model('extension/VenderCatalog/module/vendercatalog');

        $taskId = $_POST['taskId'];
        if($taskId === '{}'){

            $json['error'] = $this->language->get('empty_import_data');
            $json['result'] = 'error';
            $this->response->setOutput(json_encode($json));
        }

        $result = $this->model_extension_VenderCatalog_module_vendercatalog->removeCronTask($taskId);

        if($result){
            $json['success'] = $this->language->get('cron_task_removed');
            $json['result'] = 'success';
            $this->response->setOutput(json_encode($json));
        }
        else{
            $json['error'] = $this->language->get('cron_task_edit_error');
            $json['result'] = 'error';
            $this->response->setOutput(json_encode($json));
        }
    }
    public function GetCronTasks()
    {

        $this->load->language('extension/VenderCatalog/module/vendercatalog');
        $this->load->model('extension/VenderCatalog/module/vendercatalog');


        $result = $this->model_extension_VenderCatalog_module_vendercatalog->getCronTasks();

        if ($result) {
            $json['result'] = 'success';
            $json['data'] = $result;
            $this->response->setOutput(json_encode($json));
        } else {
            $json['error'] = $this->language->get('cron_task_edit_error');
            $json['result'] = 'error';

        }
    }

    private function translit_sef($value)
    {
        $converter = array(
            'а' => 'a',    'б' => 'b',    'в' => 'v',    'г' => 'g',    'д' => 'd',
            'е' => 'e',    'ё' => 'e',    'ж' => 'zh',   'з' => 'z',    'и' => 'i',
            'й' => 'y',    'к' => 'k',    'л' => 'l',    'м' => 'm',    'н' => 'n',
            'о' => 'o',    'п' => 'p',    'р' => 'r',    'с' => 's',    'т' => 't',
            'у' => 'u',    'ф' => 'f',    'х' => 'h',    'ц' => 'c',    'ч' => 'ch',
            'ш' => 'sh',   'щ' => 'sch',  'ь' => '',     'ы' => 'y',    'ъ' => '',
            'э' => 'e',    'ю' => 'yu',   'я' => 'ya',
        );

        $value = mb_strtolower($value);
        $value = strtr($value, $converter);
        $value = mb_ereg_replace('[^-0-9a-z]', '-', $value);
        $value = mb_ereg_replace('[-]+', '-', $value);
        $value = trim($value, '-');

        return $value;
    }

	function clearFolder($folder) {
		if ($objs = glob($folder . '/*')) {
			foreach($objs as $obj) {
				is_dir($obj) ? clear_dir($obj, true) : unlink($obj);
			}
		}

	}

	public function save(): void {
		$this->load->language('extension/VenderCatalog/module/vendercatalog');
		

		$json = [];

		if (!$this->user->hasPermission('modify', 'extension/VenderCatalog/module/vendercatalog')) {
			$json['error'] = $this->language->get('error_permission');
		}

		if (!$json) {

			$this->load->model('setting/module');

			if (!$this->request->post['module_id']) {

				$json['module_id'] = $this->model_setting_module->addModule('vender.import', $this->request->post);
			} else {
				$this->model_setting_module->editModule($this->request->post['module_id'], $this->request->post);
			}

			$json['success'] = $this->language->get('text_success');
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		
	}

	private function Debug($data){
	    echo "<pre>";
	    print_r($data);
	    echo PHP_EOL;
	    echo "</pre>";
	    exit();
    }
}
